---
author: Mireille Coilhac
title: Activités flashs
---

!!! info "Refaire les activités flashs"

    Pour chaque activité flash : 

    * Cliquer dessus pour visualiser le sujet
    * Faire un clic droit, puis sélectionner "Enregistrer la cible du lien sous" (ou un message équivalent, cela dépend des navigateurs) si vous voulez télécharger l'activité au format pdf.


* [Flash 1A : lire un coefficient directeur](a_telecharger/flash1A.pdf){ .md-button target="_blank" rel="noopener" }
* [Flash 1B : lire un coefficient directeur](a_telecharger/flash1B.pdf){ .md-button target="_blank" rel="noopener" }
* [Flash 2A : tracer une droite](a_telecharger/flash2A.pdf){ .md-button target="_blank" rel="noopener" }
* [Flash 2B : tracer une droite](a_telecharger/flash2B.pdf){ .md-button target="_blank" rel="noopener" }
* [Flash 3A : lire un nombre dérivé](a_telecharger/flash3A.pdf){ .md-button target="_blank" rel="noopener" }
* [Flash 3B : lire un nombre dérivé](a_telecharger/flash3A.pdf){ .md-button target="_blank" rel="noopener" }
