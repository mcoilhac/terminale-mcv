---
author: Mireille COILHAC
title: Nombre dérivé
---

## I. Nombre dérivé

Lire le nombre dérivé :

[Entraînement](https://fr.khanacademy.org/math/be-5eme-secondaire4h2/xe8f0cb2c937e9fc1:derivee-d-une-fonction/xe8f0cb2c937e9fc1:nombre-derive-d-une-fonction-en-un-point/e/graphs-of-functions-and-their-derivatives){ .md-button target="_blank" rel="noopener" }


## II. Notion de fonction dérivée

[Skieur](https://www.geogebra.org/material/iframe/id/QTQxkGYG/width/1280/height/800/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/true/rc/false/ld/false/sdz/true/ctl/false){ .md-button target="_blank" rel="noopener" }

<iframe scrolling="no" title="Nombre dérivé- fonction dérivée" src="https://www.geogebra.org/material/iframe/id/QTQxkGYG/width/1280/height/800/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/true/rc/false/ld/false/sdz/true/ctl/false" width="1280px" height="800px" style="border:0px;"> </iframe>