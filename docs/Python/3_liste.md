---
author: Votre nom
title: Premier élément d'une liste Python
tags:
  - 3-liste/tableau
---

🛠 Ce chapitre est en travaux. 🛠

😊 Ne pas essayer de regarder ce qui suit maintenant ...

La fonction `premier` prend en paramètres une liste Python **non vide** et renvoie le premier élément de cette liste.

???+ question "Compléter ci-dessous"

    {{ IDE('scripts/premier_liste') }}
