---
author: Votre nom
title: Une fonction
tags:
  - 2-fonction
---

🛠 Ce chapitre est en travaux. 🛠

😊 Ne pas essayer de regarder ce qui suit maintenant ...

La fonction `addition` prend en paramètres deux nombres entiers ou flottants, et renvoie la somme des deux.

???+ question "Compléter ci-dessous"

    {{ IDE('scripts/addition') }}
